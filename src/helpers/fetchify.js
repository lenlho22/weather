export const fetchify = (isFetched, content) => {
    if (!isFetched) {
        return (
            <div className = 'loading'>
                Загрузка...
            </div>
        );
    }

    if (content) {
        return content;
    }

    return null;
};
