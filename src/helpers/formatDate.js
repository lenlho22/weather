import { ru } from 'date-fns/locale';
import { format } from 'date-fns';

export const getWeekDay = (date) => format(date, 'EEEE', { locale: ru });

export const getDate = (date) => format(date, 'd MMMM', { locale: ru });
