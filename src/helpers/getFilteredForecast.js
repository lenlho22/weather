const filterFunctions = {
    dayType: (dayType) => (item) => item.type === dayType,
    minTemp: (minTemp) => (item) => item.temperature >= minTemp,
    maxTemp: (maxTemp) => (item) => item.temperature <= maxTemp,
};

export const getFilteredForecast = (filter, forecast) => {
    const settedFilters = Object.entries(filter).filter(([_, value]) => !!value);

    if (settedFilters.length > 0) {
        const result = settedFilters.reduce((acc, [key, value]) => {
            const filterFunc = filterFunctions[ key ](value);

            return acc.filter(filterFunc);
        }, forecast);

        return result;
    }

    return forecast;
};
