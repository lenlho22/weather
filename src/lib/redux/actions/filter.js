import { filterTypes as types } from '../types';

export const filterActions = Object.freeze({
    setFilter: (filter) => ({
        type:    types.SET_FILTER,
        payload: filter,
    }),
    clearFilter: () => ({
        type: types.CLEAR_FILTER,
    }),
});
