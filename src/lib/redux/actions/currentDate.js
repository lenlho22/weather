import { currentDateTypes as types } from '../types';

export const currentDateActions = Object.freeze({
    setCurrentDate: (date) => ({
        type:    types.SET_CURRENT_DATE,
        payload: date,
    }),
});
