import { currentDateTypes as types } from '../types';

const initialState = {
    id:              '',
    day:             '',
    temperature:     '',
    humidity:        '',
    rainProbability: '',
    weatherType:     '',
};

export const currentDateReducer = (state = initialState, { type, payload }) => {
    switch (type) {
    case  types.SET_CURRENT_DATE: {
        const {
            id,
            day,
            temperature,
            humidity,
            rain_probability: rainProbability,
            type: weatherType,
        } = payload;

        return {
            id,
            day,
            temperature,
            humidity,
            rainProbability,
            weatherType,
        };
    }

    default: {
        return state;
    }
    }
};
