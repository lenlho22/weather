import { filterTypes as types } from '../types';

const initialState = {
    dayType: null,
    minTemp: null,
    maxTemp: null,
};

export const filterReducer = (state = initialState, { type, payload }) => {
    switch (type) {
    case  types.SET_FILTER: {
        return {
            ...state,
            ...payload,
        };
    }

    case  types.CLEAR_FILTER: {
        return {
            ...initialState,
        };
    }

    default: {
        return state;
    }
    }
};
