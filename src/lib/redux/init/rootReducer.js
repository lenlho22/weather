// Core
import { combineReducers } from 'redux';

// Reducers
import { currentDateReducer, filterReducer } from '../reducers';

export const rootReducer = combineReducers({
    currentDate: currentDateReducer,
    filter:      filterReducer,
});
