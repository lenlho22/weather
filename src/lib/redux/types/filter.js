export const filterTypes = Object.freeze({
    SET_FILTER:   'SET_FILTER',
    CLEAR_FILTER: 'CLEAR_FILTER',
});
