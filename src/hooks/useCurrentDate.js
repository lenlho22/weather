import { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { currentDateActions } from '../lib/redux/actions';
import { selectCurrentDate } from '../lib/redux/selectors';

export const useCurrentDate = () => {
    const dispatch = useDispatch();
    const currentDate = useSelector(selectCurrentDate);

    return {
        currentDate,
        setCurrentDate: useCallback((data) => {
            if (currentDate.id !== data.id) {
                dispatch(currentDateActions.setCurrentDate(data));
            }
        }, [dispatch, currentDate]),
    };
};
