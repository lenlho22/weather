import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { filterActions } from '../lib/redux/actions';
import { selectFilter } from '../lib/redux/selectors';

export const useFilter = () => {
    const dispatch = useDispatch();
    const filter = useSelector(selectFilter);

    const [isFiltered, setIsFiltered] = useState(false);

    return {
        filter,
        isFiltered,
        setFilter: (data) => {
            setIsFiltered(true);
            dispatch(filterActions.setFilter(data));
        },
        clearFilter: () => {
            setIsFiltered(false);
            dispatch(filterActions.clearFilter());
        },
    };
};
