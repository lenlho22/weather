import { useRef, useEffect } from 'react';

export const useRenderCount = (componentName) => {
    const renderCount = useRef(1);

    useEffect(() => {
        renderCount.current += 1;
    });

    if (componentName) {
        console.log(`[${componentName}] was rendered ${renderCount.current} times`);
    }

    return { renderCount: renderCount.current };
};
