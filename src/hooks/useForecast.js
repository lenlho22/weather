// @ts-nocheck
/* Core */
import { useQuery, useQueryClient } from 'react-query';

/* Other */
import { api } from '../api/api';

export const useForecast = () => {
    const queryClient = useQueryClient();

    const query = useQuery('forecast', api.getWeather,
        {
            initialData: () => queryClient.getQueryData('forecast'),
            staleTime:   60 * 60 * 1000, // I cannot see how it works
        });

    return query;
};
