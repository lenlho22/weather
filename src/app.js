// Core
import { useEffect, useState } from 'react';

// Instruments
import { fetchify, getFilteredForecast } from './helpers';
import { useForecast, useCurrentDate, useFilter } from './hooks';

// Components
import { CurrentWeather } from './components/CurrentWeather';
import { Forecast } from './components/Forecast';
import { Filter } from './components/Filter';

const MAX_FORECAST_LENGTH = 7;

export const App = () => {
    const { filter } = useFilter();
    const { setCurrentDate } = useCurrentDate();
    const [forecastItems, setForecastItems] = useState([]);
    const {
        data: forecast, isFetched, isFetchedAfterMount, isError, refetch,
    } = useForecast();

    useEffect(() => {
        if (isFetchedAfterMount && Array.isArray(forecast)) {
            setCurrentDate(forecast[ 0 ]);
            setForecastItems(forecast.slice(0, MAX_FORECAST_LENGTH));
        }
    }, [isFetchedAfterMount]);

    useEffect(() => {
        if (!forecast) return;

        const filteredForecast = getFilteredForecast(filter, forecast);

        if (filteredForecast.length > 0) {
            setCurrentDate(filteredForecast[ 0 ]);
        }

        setForecastItems(filteredForecast.slice(0, MAX_FORECAST_LENGTH));
    }, [filter]);

    const appJSX = (
        <>
            <Filter />
            { forecastItems.length > 0 && <CurrentWeather /> }
            <Forecast items = { forecastItems } />
        </>
    );

    return (
        <main>
            { isError
                ? <div className = 'loading'>Что-то пошло не так...</div>
                : fetchify(isFetched, appJSX) }
        </main>
    );
};

