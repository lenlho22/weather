import { memo } from 'react';

import { getWeekDay } from '../../helpers';
import { useRenderCount } from '../../hooks';

export const ForecastItem = memo(({
    isSelected, onClick, ...item
}) => {
    const { renderCount } = useRenderCount();

    const {
        id, day, type: weatherType, temperature,
    } = item;
    const selectedClassName = isSelected ? 'selected' : '';
    const classNames = `day ${weatherType} ${selectedClassName}`;

    console.log(`
item with id = ${id}
----> ${new Date(day).toLocaleDateString('ru-RU', {
        weekday: 'long',
    })} was rendered ${renderCount} times

    `);

    return (
        <div className = { classNames } onClick = { () => onClick(item) }>
            <p>{ getWeekDay(day) }</p>
            <span>{ temperature }</span>
        </div>
    );
});

ForecastItem.displayName = 'ForecastItem';
