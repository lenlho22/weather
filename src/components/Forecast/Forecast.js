import { useCurrentDate } from '../../hooks';

import { ForecastItem } from './ForecastItem';

export const Forecast = ({ items = [] }) => {
    const { currentDate, setCurrentDate } = useCurrentDate();

    return (
        <div className = 'forecast'>
            { items.length > 0 ? items.map((item) => (
                <ForecastItem
                    key = { item.id }
                    isSelected = { item.id === currentDate.id }
                    onClick = {  setCurrentDate }
                    { ...item } />
            )) : (
                <p className = 'message'>По заданным критериям нет доступных дней!</p>
            ) }
        </div>
    );
};
