import { memo } from 'react';
import { useForm } from 'react-hook-form';

import { useRenderCount, useFilter } from '../../hooks';

const defaultValues = {
    dayType: null,
    minTemp: '',
    maxTemp: '',
};

export const Filter = memo(() => {
    useRenderCount('Filter');

    const { isFiltered, setFilter, clearFilter } = useFilter();

    const form = useForm({
        defaultValues,
        mode: 'onSubmit',
    });

    const handleSubmit = form.handleSubmit((data) => {
        setFilter(data);
    });

    const handleReset = () => {
        form.reset();
        clearFilter();
    };

    return (
        <form
            className = 'filter' onSubmit = { handleSubmit }>
            <div>
                <input
                    disabled = { isFiltered }
                    type = 'radio'
                    id = 'cloudy'
                    name = 'dayType'
                    value = 'cloudy'
                    { ...form.register('dayType') } />
                <label htmlFor = 'cloudy'>
                    <span className = 'checkbox'>Облачно</span>
                </label>
            </div>

            <div>
                <input
                    disabled = { isFiltered }
                    type = 'radio'
                    id = 'sunny'
                    name = 'dayType'
                    value = 'sunny'
                    { ...form.register('dayType') } />
                <label htmlFor = 'sunny'>
                    <span className = 'checkbox'>Солнечно</span>
                </label>
            </div>

            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature'>Минимальная температура</label>
                <input
                    id = 'min-temperature'
                    type = 'number'
                    disabled = { isFiltered }
                    { ...form.register('minTemp', { valueAsNumber: true }) } />
            </p>
            <p className = 'custom-input'>
                <label htmlFor = 'max-temperature'>Максимальная температура</label>
                <input
                    id = 'max-temperature'
                    type = 'number'
                    disabled = { isFiltered }
                    { ...form.register('maxTemp', { valueAsNumber: true }) } />
            </p>
            { isFiltered
                ? <button type = 'button' onClick = { handleReset }>Сбросить</button>
                : <button type = 'submit' disabled = { !form.formState.isDirty }>Отфильтровать</button>
            }
        </form>
    );
});
