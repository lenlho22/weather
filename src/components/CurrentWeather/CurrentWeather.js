import { getWeekDay, getDate } from '../../helpers';
import { useRenderCount, useCurrentDate } from '../../hooks';


export const CurrentWeather = () => {
    useRenderCount('CurrentWeather');

    const { currentDate } = useCurrentDate();
    const {
        id, temperature, humidity,  rainProbability, weatherType, day,
    } = currentDate;

    if (!id) {
        return null;
    }

    return (
        <>
            <div className = 'head'>
                <div className = { `icon ${weatherType}` } />
                <div className = 'current-date'>
                    <p>{ getWeekDay(day) }</p>
                    <span>{ getDate(day) }</span>
                </div>
            </div>
            <div className = 'current-weather'>
                <p className = 'temperature'>{ temperature }</p>
                <p className = 'meta'>
                    <span className = 'rainy'>{ `%${rainProbability}` }</span>
                    <span className = 'humidity'>{ `%${humidity}` }</span>
                </p>
            </div>
        </>
    );
};
